var animateMove;
var position = 0;
const MAX_TIME = 8;
var reset = false;
var runtime = new Date();
var intialtime = new Date();
var i = 1;
var j = 101;
var totalDist = 1000;
var totalTime;
var dist;
var INTERVAL = 100;
var runner;

function init() {
    console.log('init');
    document.getElementById('bg').style.left = '0px';
    document.getElementById('bg').style.position = 'relative';
}
function startAnimation() {
    var run = new Run();
    startTime = new Date().getTime();
    if (reset) run.stop();
        i = 1;
        j = 101;
        dist = 1000;
        totalDist = 1000;
        totalTime = (Math.floor(Math.random() * 8) + 3) * 1000;
        console.log(totalTime);
        document.getElementById('go').disabled = true;
        runner = document.getElementById('bg')
}
class Run 
{
    moveRight() {
        console.log('moveright');
        var animateMove = setInterval(() => {
            // Make runner move forward
                var curTime = new Date().getTime();
                var timePassed = curTime - startTime;
                var timeRemain = totalTime - timePassed;

                var curPosition = Number(runner.style.left.replace('px', ''));
                var distToCover = totalDist - curPosition;

                var distTomove = (distToCover * INTERVAL) / timeRemain;

                runner.style.left = (timeRemain < 0 ? 1000 : (curPosition + distTomove)) + 'px';

            clearInterval(animateMove);               
            this.changeButton();

            for(let i=1; i<10; i++) {
                let j = i;
                j--;
            }

            //Fill the table information
                console.log(curPosition * timePassed);
                document.getElementById(i).textContent = timePassed;
                i++;
                document.getElementById(j).textContent = dist;
                j++;
                dist += 1000;
                if (i > 6) document.getElementById(i - 7).scrollIntoView();
            //Make runner image animation  as running
            document.getElementById('runner').style.backgroundPosition = `-${position}px 0px`;
            if (position < 104) {
                position += 26;
            }
            position = 0;
        }, INTERVAL)      
    }
    changeButton() {
        reset = true;
    }
    resetButton() {
        document.getElementById('go').value = 'GO';
        document.getElementById('go').disabled = false;
        reset = false;
        i = 1;
        j = 101;
            document.getElementById(i).textContent = "---";
            i++;
            document.getElementById(j).textContent = "---";
            j++;
        document.getElementById('0').scrollIntoView();
    }
}
window.onload = init;